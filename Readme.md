## Cucumbersome

#### Run those infinitely boring tests with a bit of joy.

Cucumbersome runs any command line program amusing you in the meanwhile with some funny quotes. For now, the package ```fortune``` is required (on Ubuntu: ```apt-get install fortune```).

To run cucumbersome:

```
$ ruby cucumbersome.rb cucumber -c ../path/to/cucumber/feature 
```
