require 'curses'
load 'matcher.rb'
include Curses

Curses.init_screen
Curses.start_color
Curses.cbreak
Curses.init_pair(COLOR_YELLOW, COLOR_YELLOW, COLOR_BLUE)
Curses.init_pair(COLOR_WHITE, COLOR_WHITE, COLOR_BLACK)
Curses.init_pair(COLOR_RED, COLOR_RED, COLOR_BLACK)
Curses.init_pair(COLOR_BLUE, COLOR_BLUE, COLOR_BLACK)
Curses.init_pair(COLOR_GREEN, COLOR_GREEN, COLOR_BLACK)


$term_width, $term_height = Curses.cols, Curses.lines

output = Curses::Window.new($term_height - 10, $term_width, 10, 0)
output.scrollok true
banner = Curses::Window.new(10, $term_width, 0, 0)
banner.box(?|, ?-)
banner.attrset(color_pair(COLOR_YELLOW)|A_BOLD)
banner.setpos(0, 0)
banner.clear
10.times do
  banner.addstr (" " * $term_width) 
end
banner.refresh
banner_msg = Curses::Window.new(6, $term_width-4, 2, 2)
banner_msg.attrset(color_pair(COLOR_YELLOW)|A_BOLD)
banner_msg.bkgdset(' '.bytes.first |  color_pair(COLOR_YELLOW))

Thread.new do
  # fg = Curses.init_color(1, 100, 0, 50)
  # bg = Curses.init_color(2, 40, 40, 40)
  # Curses.init_pair(0, 0, 0)
  # Curses.color_pair(0)
  while true
    banner_msg.clear
    banner_msg.setpos(0, 0)
    msg = `fortune`.split("\n")
    msg += [].fill("", 0, 6-msg.count)
    msg.each do |line|
      line.gsub!("\t", "    ")
      line.chomp!
      line += " " * ($term_width - 4 - line.length)
      banner_msg.addstr line
    end
    banner_msg.refresh
    banner_msg.getch
  end
end

##
$command = ARGV.join(" ")

pipe = IO.popen($command)

ansi_to_pair = {'[0m' => COLOR_WHITE, '[31m' => COLOR_RED, '[90m' => COLOR_BLUE, '[36m' => COLOR_GREEN}

while line = pipe.gets
  segments = Matcher::segments(line)
  segments.each do |segment|
    colorp = ansi_to_pair[segment[0]] || COLOR_WHITE 
    text = segment[1]
    output.attron(color_pair(colorp)|A_NORMAL) do
      output.addstr(text)
    end
  end
  output.addstr("\n")
  output.refresh
end

