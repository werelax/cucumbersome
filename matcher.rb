module Matcher
  def self.segments(line)
    remaining = line
    regex = /(\[\d\d?m)([^\[\\]+)/
    segments = []
    while remaining =~ regex
      m = remaining.match regex
      segments << ['[0m', m.pre_match[0..-2]] if remaining == line
      code = m[1]
      text = m[2][0..-2]
      remaining = m.post_match
      segments << [code, text]
    end
    segments << ['[0m', remaining[0..-2]] unless remaining =~ /^\[\d\d?m$/
    return segments
  end
end


